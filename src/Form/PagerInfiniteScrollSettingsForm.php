<?php

namespace Drupal\pager_infinite_scroll\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Settings form for pager_infinite_scroll.
 */
class PagerInfiniteScrollSettingsForm extends ConfigFormBase {
  use StringTranslationTrait;

  /**
   * Configuration name.
   *
   * @var string
   */
  const SETTINGS = 'pager_infinite_scroll.settings';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['routes'] = [
      '#title' => $this->t('routes'),
      '#type' => 'textarea',
      '#default_value' => $config->get('routes'),
      '#required' => FALSE,
      '#description' => $this->t('The drupal urls/routes where to apply those settings. One url/route per line. The settings will be applyed on page where the routes match and contain a pager too. To load your settings on drupal frontpage(s) use the placeholder <front>.'),
    ];

    // Settings :
    $form['pager_selector'] = [
      '#title' => $this->t('Pager selector'),
      '#type' => 'textfield',
      '#default_value' => $config->get('pager_selector'),
      '#required' => TRUE,
      '#description' => $this->t('The jQuery wrapper selector of the pager'),
    ];

    $form['next_selector'] = [
      '#title' => $this->t('Next selector'),
      '#type' => 'textfield',
      '#default_value' => $config->get('next_selector'),
      '#required' => TRUE,
      '#description' => $this->t("The jQuery wrapper selector of the next page link's in the pager"),
    ];
    $form['content_selector'] = [
      '#title' => $this->t('Content selector'),
      '#type' => 'textfield',
      '#default_value' => $config->get('content_selector'),
      '#required' => TRUE,
      '#description' => $this->t('The jQuery wrapper selector of the content, where to append the next items'),
    ];

    $form['items_selector'] = [
      '#title' => $this->t('Items selector'),
      '#type' => 'textfield',
      '#default_value' => $config->get('items_selector'),
      '#description' => $this->t('The jQuery wrapper selector of the items of the list inside the content selector. Content selector + items selector must be valid.'),
    ];

    $form['load_more'] = [
      '#title' => $this->t('Use a button to load more results instead of autoload on scroll.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('load_more'),
      '#required' => FALSE,
    ];
    $form['load_more_markup'] = [
      '#title' => $this->t('Load more button markup'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('load_more_markup'),
      '#description' => $this->t('The markup for the javascript button "load more". @label will be replaced with the result of$this->t("Load more").'),
      '#states' => [
      // Hide the settings when the load more checkbox is disabled.
        'invisible' => [
          ':input[name="load_more"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['ajax_pager'] = [
      '#title' => $this->t('I just want the pager visible and in ajax (like views ajax).'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('ajax_pager'),
      '#required' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pager_infinite_scroll';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::SETTINGS,
    ];
  }

  /**
   * Final submit handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $form_state->cleanValues()->getValues();
    $config = $this->configFactory->getEditable(static::SETTINGS);
    foreach ($settings as $key => $value) {
      $config->set($key, $value);
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
